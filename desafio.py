# -*- coding: utf-8 -*-
"""
# :**AUDSAT**: Desafio de Programação em Python

### *Sistema de Informações Geográficas (SIG) & Sensoriamento Remoto*

- **:Autor:** Rafael Augusto de Mattos
- **:E-mail:** rafamattos@alumni.usp.br
- **:Disponibilização:** 14-Out-2018

"""
# Importação de ferramentas de compatibilidade Python 2/3.
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

# Importação minimalista de ferramentas essenciais.
from rasterio import open as rioepn
from json import load as jsload
from rasterio.io import MemoryFile
from rasterio.mask import mask
from os.path import realpath, normpath, splitext, basename
from os.path import join as joinpath
from os.path import split as splitpath
from glob import glob
from numpy import int32, float32, nan, nanmean
from csv import writer as csvwriter
from datetime import datetime as dtm
from warnings import filterwarnings
from sys import version_info

# Supressão de mensagens de alerta.
filterwarnings("ignore", category=RuntimeWarning)


# DESAFIO (#1) ======================================================= #
# ==================================================================== #
def ndbands(dataimg, bands, saveimg=False, savepath=None):
    """
    Computar diferença normalizada (ND - **Normalized Difference**)
    entre duas bandas quaisquer de uma imagem GeoTiff.

    Parâmetros
    ----------
    dataimg : ``DatasetReader`` (pacote *rasterio*)
              Conjunto da imagem analítica GeoTiff aberta em modo de
              leitura 'r'.
    bands   : ``tuple``
              Par de índices ordenados referentes as duas bandas da
              imagem para diferenciação.
              (ex: (4, 3) ou (1, 2) ou ...)
    saveimg : ``boolean``
              Ativação de arquivamento da imagem resultante do processo
              de diferenciação normalizada. Se ``False`` (default), a
              imagem não é arquivada, apenas retornada e permanece em
              memória volátil (cache).
    savepath : ``string``
              Caminho completo absoluto (diretório + nomenclatura) para
              arquivamento da imagem resultante. Se ``None`` (default) e
              ``saveimg=True``, o caminho utilizado é aquele mesmo
              original do conjunto ``DatasetReader`` de entrada, cuja
              nomenclatura do arquivo é preservada a menos do acréscimo
              do sufixo *"_nd"* ao final do nome.

    Retornos
    --------
    ndimg : ``DatasetReader`` (pacote *rasterio*)
            Conjunto da imagem ND GeoTiff resultante do processo de
            diferenciação normalizada.

    Notas
    -----

    + A chamada *diferença normalizada* (ND - **Normalized Difference**)
      é computada entre quaisquer duas bandas A e B na forma:

          ``ND = (BAND_A – BAND_B) / (BAND_A + BAND_B)``

      Considerando, por exemplo, o parâmetro de entrada ``bands=(4,3)``,
      ``BAND_A`` assume papel da banda número 4 enquanto ``BAND_B``
      assume papel da banda número 3, ambas presentes no conjunto
      ``dataimg`` da imagem analítica de entrada.

    + A representação em memória da imagem GeoTiff fornecida através do
      parâmetro de entrada ``dataimg`` é aquela oferecida pelo objeto
      ``DatasetReader`` disponibilizado via pacote *rasterio*.

    + O conjunto ``ndimg`` da imagem retornada pela função preserva a
      parte pertinente do perfil de metadados do conjunto de entrada
      tais como ``driver``, ``dtype``, ``crs``, entre outros.

    Exemplo
    -------
    >>> import rasterio as rio
    >>> # leitura da imagem analítica GeoTiff.
    >>> dataimg = rio.open("C:\\images\\geotiffimgexample.tif")
    >>> # computação da ND entre bandas 4 e 3, com exportação da imagem
    >>> # para 'C:\\images\\geotiffimgexample_nd.tif'.
    >>> ndimg = ndbands(
    ...    dataimg=dataimg,
    ...    bands=(4, 3),
    ...    saveimg=True)
    >>> print(repr(ndimg.name))
    'C:\\images\\geotiffimgexample_nd.tif'

    """

    # Dimensões (linhas e colunas) da imagem analítica.
    ndrows = dataimg.height
    ndcols = dataimg.width

    # Computação da diferença normalizada entre as bandas de interesse.
    # Previamente às operações, os datos são convertidos para arrays do
    # tipo ``numpy.int32`` para evitar estouro de representação. Os valores
    # das diferenças são retornados como ``numpyfloat32``.
    ndvalue = (
        (dataimg.read(bands[0]).astype(int32) -
         dataimg.read(bands[1]).astype(int32)) /
        (dataimg.read(bands[0]).astype(int32) +
         dataimg.read(bands[1]).astype(int32))
    ).reshape((1, ndrows, ndcols)).astype(float32)

    # Reutilização e atualização de metadados para compor a imagem ND.
    metainfo = dataimg.meta.copy()
    metainfo.update({
        "count": 1,
        "dtype": ndvalue.dtype,
    })

    # Avaliação condicional conforme declaração de exportação da imagem
    # ND para arquivo. Se ``saveimg=True``, são utilizados os métodos
    # padrões de escrita de imagens oferecidos pelo pacote *rasterio*.
    # Neste caso, a imagem é salva em disco e retornada em modo leitura
    # "r" pela função. Se ``saveimg=False``, é utilizada a representação
    # em memória oferecida pelo objeto ``MemoryFile()``. Neste caso, a
    # imagem não é salva em disco, permanece "viva" na memória volátil
    # (cache).
    #
    if saveimg:
        # Determinação de nomenclatura do arquivo da imagem ND.
        if savepath is None:
            # Identificação de diretório e nome do arquivo da imagem
            # analítica para reuso.
            imgpath, imgfilename = splitpath(dataimg.name)

            # Adição do sufixo "_nd" ao nome original.
            ndimgfilename = joinpath(imgpath,
                                     splitext(imgfilename)[0] +
                                     "_nd" +
                                     splitext(imgfilename)[1])
        else:
            ndimgfilename = savepath

        # Escrita em disco do arquivo da imagem ND.
        with rioepn(ndimgfilename, "w", **metainfo) as dataset:
            dataset.write(ndvalue)
    else:
        # Escrita em memória volátil da imagem ND.
        ndimgfile = MemoryFile()
        ndimgfilename = ndimgfile.name

        with ndimgfile.open(**metainfo) as dataset:
            dataset.write(ndvalue)

    # Retorno do conjunto de dados já aberto para uso imediato.
    dataset = rioepn(ndimgfilename, "r", **metainfo)

    return dataset


# DESAFIO (#2) ======================================================= #
# ==================================================================== #
def clipnd(dataimg, gjsonfp, saveimg=False, savepath=None):
    """
    Recortar imagem ND GeoTiff em determinado limite espacial.

    Parâmetros
    ----------
    dataimg : ``DatasetReader`` (pacote *rasterio*)
              Conjunto da imagem ND GeoTiff aberta em modo de leitura
              'r' a ser submetida ao corte.
    gjsonfp : ``file object``
              Arquivo vetorial geojson aberto em modo de leitura 'r'
              contendo as informações do limite espacial de corte.
    saveimg : ``boolean``
              Ativação de arquivamento da imagem resultante do processo
              de recorte. Se ``False`` (default), a imagem não é
              arquivada, apenas retornada e permanece em memória volátil
              (cache).
    savepath : ``string``
              Caminho completo absoluto (diretório + nomenclatura) para
              arquivamento da imagem resultante. Se ``None`` (default) e
              ``saveimg=True``, o caminho utilizado é aquele mesmo
              original do conjunto ``DatasetReader`` de entrada, cuja
              nomenclatura do arquivo é preservada a menos do acréscimo
              do sufixo *"_clp"* ao final do nome.

    Retornos
    --------
    clpimg : ``DatasetReader`` (pacote *rasterio*)
             Conjunto da imagem GeoTiff resultante do recorte espacial
             dentro do limite informado pelo arquivo vetorial geojson.

    Notas
    -----

    + A representação em memória da imagem GeoTiff fornecida através do
      parâmetro de entrada ``dataimg`` é aquela oferecida pelo objeto
      ``DatasetReader`` disponibilizado via pacote *rasterio*.

    + O arquivo-objeto vetorial do tipo geojson, fornecida através do
      parâmetro de entrada ``gjsonfp`` e contendo as informações
      geográficas do limite espacial da área de corte, deve possuir um
      atributo primário chamado "features" contendo uma lista. Por sua
      vez, o primeiro elemento (índice 0) desta lista deve possuir um
      atributo primário "geometry" cujo conteúdo representa os limites
      espaciais de recorte no formato exigido pelo parâmetro de entrada
      ``shapes`` da função *rasterio.mask.mask()*.

    + O conjunto ``clpimg`` da imagem retornada pela função preserva a
      parte pertinente do perfil de metadados do conjunto de entrada
      tais como ``driver``, ``dtype``, ``crs``, entre outros.

    Exemplo
    -------
    >>> import rasterio as rio
    >>> # leitura da imagem GeoTiff a ser recortada.
    >>> dataimg = rio.open("C:\\images\\geotiffimgexample.tif")
    >>> # leitura do arquivo vetorial geojson com os limites espaciais.
    >>> gjsonfp = open("C:\\images\\gjsonlimexample.geojson")
    >>> # computação do recorte, com exportação da imagem para
    >>> # 'C:\\images\\geotiffimgexample_clp.tif'.
    >>> clpimg = clipnd(
    ...    dataimg=dataimg,
    ...    gjsonfp=gjsonfp,
    ...    saveimg=True)
    >>> print(repr(clpimg.name))
    'C:\\images\\geotiffimgexample_clp.tif'

    """

    # Leitura do arquivo vetorial geojson. São garantidos a leitura
    # desde o início do arquivo (caso já tenha sido lido anteriormente
    # no escopo externo à função) e reinicio do apontamento ao início do
    # arquivo.
    gjsonfp.seek(0)
    gjdata = jsload(gjsonfp)
    gjsonfp.seek(0)

    # Computação do recorte.
    dataclp, datatfrm = mask(dataimg, [gjdata["features"][0]["geometry"]],
                             nodata=nan)

    # Reutilização e atualização de metadados para compor a imagem
    # submetida ao recorte.
    metainfo = dataimg.meta.copy()
    metainfo.update({
        "transform": datatfrm,
    })

    # Avaliação condicional conforme declaração de exportação da imagem
    # recortada para arquivo. Se ``saveimg=True``, são utilizados os
    # métodos padrões de escrita de imagens oferecidos pelo pacote
    # *rasterio*. Neste caso, a imagem é salva em disco e retornada em
    #  modo leitura "r" pela função. Se ``saveimg=False``, é utilizada a
    #  representação em memória oferecida pelo objeto ``MemoryFile()``.
    #  Neste caso, a imagem não é salva em disco, permanece "viva" na
    # memória volátil (cache).
    #
    if saveimg:
        # Determinação de nomenclatura do arquivo da imagem ND.
        if savepath is None:
            # Identificação de diretório e nome do arquivo da imagem
            # analítica para reuso.
            imgpath, imgfilename = splitpath(dataimg.name)

            # Adição do sufixo "_nd" ao nome original.
            ndimgfilename = joinpath(imgpath,
                                     splitext(imgfilename)[0] +
                                     "_clp" +
                                     splitext(imgfilename)[1])
        else:
            ndimgfilename = savepath

        # Escrita em disco do arquivo da imagem recortada.
        with rioepn(ndimgfilename, "w", **metainfo) as dataset:
            dataset.write(dataclp)
    else:
        # Escrita em memória volátil da imagem recortada.
        ndimgfile = MemoryFile()
        ndimgfilename = ndimgfile.name

        with ndimgfile.open(**metainfo) as dataset:
            dataset.write(dataclp)

    # Retorno do conjunto de dados já aberto para uso imediato.
    dataset = rioepn(ndimgfilename, "r", **metainfo)

    return dataset


# DESAFIO (#3) ======================================================= #
# ==================================================================== #
def meanpxlnd(dataimg, gjsonfp=None):
    """
    Computar valor médio dos pixels de uma imagem ND GeoTiff.

    Parâmetros
    ----------
    dataimg : ``DatasetReader`` (pacote *rasterio*)
              Conjunto da imagem ND GeoTiff aberta em modo de leitura
              'r' a ser submetida à computação da média.
    gjsonfp : ``file object``
              Arquivo vetorial geojson aberto em modo de leitura 'r'
              contendo as informações do limite espacial de corte. Se
              ``None`` (default), nenhum recorte é realizado.

    Retornos
    --------
    meanv : ``float`` (pacote *rasterio*)
            Valor médio dos pixels da imagem ND ou somente daqueles
            contidos dentro do limite espacial de recorte.

    Notas
    -----

    + A representação em memória da imagem GeoTiff fornecida através do
      parâmetro de entrada ``dataimg`` é aquela oferecida pelo objeto
      ``DatasetReader`` disponibilizado via pacote *rasterio*.

    + O arquivo-objeto vetorial do tipo geojson, fornecida através do
      parâmetro de entrada ``gjsonfp`` e contendo as informações
      geográficas do limite espacial da área de corte, deve possuir um
      atributo primário chamado "features" contendo uma lista. Por sua
      vez, o primeiro elemento (índice 0) desta lista deve possuir um
      atributo primário "geometry" cujo conteúdo representa os limites
      espaciais de recorte no formato exigido pelo parâmetro de entrada
      ``shapes`` da função *rasterio.mask.mask()*.

    + É possível fornecer a imagem ND já cortada em determinado limite
      espacial. Neste caso, o arquivo vetorial é dispensável
      (``gjsonfp=None``). Ou pode-se fornecer a imagem ND íntegra (sem
      cortes) a qual será recortada no limite fornecido via arquivo
      vetorial, sendo este de declaração obrigatória no parâmetro
      ``gjsonfp`` neste caso.

    Exemplo
    -------
    >>> import rasterio as rio
    >>> # leitura da imagem GeoTiff íntegra (sem recortes).
    >>> dataimg = rio.open("C:\\images\\geotiffimgexample.tif")
    >>> # leitura do arquivo vetorial geojson com os limites espaciais.
    >>> gjsonfp = open("C:\\images\\gjsonlimexample.geojson")
    >>> # computação do valor médio dos pixels dentro do recorte.
    >>> meanv = meanpxlnd(
    ...    dataimg=dataimg,
    ...    gjsonfp=gjsonfp)
    >>> print(meanv)
    0.33413002

    Resultado equivalente ao anterior via outro procedimento:

    >>> # computação do recorte da imagem íntegra.
    >>> clpimg = clipnd(
    ...    dataimg=dataimg,
    ...    gjsonfp=gjsonfp,
    ...    saveimg=False)
    >>> # computação do valor médio dos pixels da imagem já recortada.
    >>> meanv = meanpxlnd(
    ...    dataimg=clpimg)
    >>> print(meanv)
    0.33413002

    """

    # Carregamento dos valores dos pixels.
    dataclp = dataimg.read(1)

    # Recorte condicional conforme declaração de arquivo vetorial.
    if gjsonfp is not None:

        # Leitura do arquivo vetorial geojson. São garantidos a leitura
        # desde o início do arquivo (caso já tenha sido lido
        # anteriormente no escopo externo à função) e reinicio do
        # apontamento ao início do arquivo.
        gjsonfp.seek(0)
        gjdata = jsload(gjsonfp)
        gjsonfp.seek(0)

        # Computação do recorte.
        dataclp, datatfrm = mask(dataimg, [gjdata["features"][0]["geometry"]],
                                 nodata=nan)

    # Retorno do valor médio dos pixels desprezando-se NaNs presentes na
    # área externa ao limite espacial de recorte.
    return nanmean(dataclp)


# DESAFIO (#4) ======================================================= #
# ==================================================================== #
def main():
    """ Núcleo de computação principal. """

    # Parâmetros de Entrada ========================================== #
    # ================================================================ #

    # Diretório de localização das imagens GeoTiff.
    # (relativo ao diretório de localização deste script)
    imgsrelpath = r"..\IMAGENS_PLANET"

    # Diretório de localização + nomenclatura do arquivo vetorial
    # geojson com os limites espaciais de recorte.
    # (relativo ao diretório de localização deste script)
    gjsonrelpath = r"..\gleba01.geojson"

    # Diretório de localização + nomenclatura do arquivo CSV de saída
    # com os valores médios de NDVI.
    # (relativo ao diretório de localização deste script)
    csvrelpath = r"..\ndvis.csv"

    # Codificação de caracteres (encoding) do arquivo CSV.
    csvencoding = "utf-8"

    # Impressão de log de execução em tela. Se ``True``, mensagens das
    # etapas de processamento das imagens são informadas na tela via
    # janela de console.
    logstdout = True

    # Manipulação & Análise dos Dados ================================ #
    # ================================================================ #

    # Diretório absoluto de localização das imagens.
    imgsabspath = normpath(joinpath(realpath(__file__), imgsrelpath))

    # Diretório absoluto de localização + nomenclatura do arquivo
    # vetorial geojson.
    gjsonabspath = normpath(joinpath(realpath(__file__), gjsonrelpath))

    # Abertura do arquivo vetorial geojson contendo os limites espaciais
    # (talhão agrícola) para recorte das imagens.
    gjsonfp = open(gjsonabspath, "r")

    # Diretório absoluto de localização + nomenclatura do arquivo CSV.
    csvabspath = normpath(joinpath(realpath(__file__), csvrelpath))

    # Abertura do arquivo CSV para exportação de valores médios de NDVI
    # e instanciação do escritor especializado CSV.
    # (controle de compatibilidade Python 2/3)
    if version_info[0] >= 3:
        csvfp = open(csvabspath, "w", newline="", encoding=csvencoding)
        csvfpw = csvwriter(csvfp, delimiter=",", quotechar='"')
    else:
        csvfp = open(csvabspath, "bw")
        csvfpw = csvwriter(csvfp, delimiter=",".encode(csvencoding),
                           quotechar='"'.encode(csvencoding))

    # Para cada imagem analítica GeoTiff de extensão "*.tif" encontrada
    # no diretório declarado acima:
    #    (#1) aplica-se o algoritmo NDVI (Normalized Difference
    #         Vegetation Index) via função ``ndbands``;
    #    (#2) recorta-se a imagem NDVI resultante no limite espacial de
    #         interesse via função ``clipnd``;
    #    (#3) computa-se o valor médio dos pixels da imagem NDVI
    #         recortada no limite espacial;
    #    (#4) armazena-se em arquivo CSV o valor médio de NDVI computado
    #         com formatação par-valor *data da imagem,valor médio*.
    #
    for geofn in (glob(joinpath(imgsabspath, "*.tif"))):

        if logstdout:
            print("Processando imagem \"{:s}\"...".format(basename(geofn)),
                  end="")

        try:
            # Carregamento da imagem analítica GeoTiff.
            with rioepn(geofn, mode="r") as dataset:

                # Criação da imagem NDVI utilizando as bandas
                # *infravermelho próximo* (banda 4) e *vermelho*
                # (banda 3).
                datasetndvi = ndbands(dataimg=dataset,
                                      bands=(4, 3),
                                      saveimg=False)

                # Recorte da imagem NDVI no limite espacial fornecido
                # pelo arquivo vetorial geojson.
                datasetclp = clipnd(dataimg=datasetndvi,
                                    gjsonfp=gjsonfp,
                                    saveimg=False)

                # Computação do valor médio de NDVI, sobre a imagem
                # recortada, dentro do limite espacial.
                meanndvi = meanpxlnd(dataimg=datasetclp)

                # Exportação para arquivo CSV do valor médio de NDVI
                # atrelado à data da imagem (presente no nome do
                # arquivo). Especifica-se explicitamente formatação da
                # data e do valor médio para facilitar customizações de
                # interesse.
                imgtime = dtm.strptime(basename(geofn).split("_")[0], "%Y%m%d")

                csvfpw.writerow([imgtime.strftime("%Y-%m-%d"),
                                 "{:.4f}".format(meanndvi)])

                # Fechamento dos arquivos de imagens gerados.
                datasetndvi.close()
                datasetclp.close()
        except Exception as err:
            if logstdout:
                print("[erro]")

            # Captura e exibição de exceção.
            print(2 * "=" + "> ", end="")
            print(err)
        else:
            if logstdout:
                print("[ok]")

    # Fechamento dos arquivos vetorial geojson e CSV.
    gjsonfp.close()
    csvfp.close()


if __name__ == "__main__":
    main()
