# :**AUDSAT**: Desafio de Programação em Python
#### *Sistema de Informações Geográficas (SIG) & Sensoriamento Remoto*

- [x] **:Autor:** Rafael Augusto de Mattos
- [x] **:E-mail:** rafamattos@alumni.usp.br
- [x] **:Disponibilização:** 14-Out-2018

## Informações importantes

#### Ambiente de desenvolvimento

O desafio foi implementado e testado em ambientes de desenvolvimento virtuais
**Python** (versões **2** & **3**) construídos a partir da ferramenta
[**Conda**](https://conda.io/docs/) -- *"Open source package management system
and environment management system"*, oferecida pela **Continuum Analytics,
Inc.** (dba Anaconda, Inc.) [https://www.anaconda.com]. Esta ferramenta é da
mesma família de soluções de admnistração de pacotes e ambientes virtuais,
inclusas aquelas mais populares
[**virtualenv**](https://virtualenv.pypa.io/en/stable/) e
[**pipenv**](https://pipenv.readthedocs.io/en/latest/), e está inclusa em todas
as versões das distribuições
[Anaconda](https://www.anaconda.com/download/)<sup>®</sup>
([sobre](https://conda.io/docs/glossary.html#anaconda-glossary)) e
[Miniconda](https://conda.io/miniconda.html)
([sobre](https://conda.io/docs/glossary.html#miniconda-glossary)).

Os pacotes utilizados em cada ambiente encontram-se listados nos arquivos de
especificações **"condaenvpkgs\__py2_.txt"** e **"condaenvpkgs\__py3_.txt"**
disponibilizados junto ao repositório e associados às versões **2** e **3** do
Python, respectivamente. O código-fonte do desafio é compatível com ambas.

Para recriar os ambientes *ipsis litteris*, basta executar o comando no prompt:

```dos
conda create --name <env> --file <file>
```
onde ```<env>``` é o nome do ambiente recriado e ```<file>``` é o arquivo de
especificações contendo os pacotes e versões a serem instalados no ambiente.

#### Configurações e código-fonte

Todo desafio está contido no *script file* **"desafio.py"**.

Algumas opções de configuração passíveis de customização, como por exemplo:
localização dos arquivos de entrada (imagens + talhão agrícola); localização e
nomenclatura dos produtos gerados (imagens processadas + valores médios de
NDVI); etc, encontram-se no início da função `main()` junto à seção
**_# Parâmetros de Entrada #_**.

Vale destacar que, por *default*, as imagens GeoTiff processadas resultantes do
cálculo do algoritmo NDVI e do recorte espacial não são armazenadas em disco.
Utiliza-se sua representação diretamente em memória volátil (*cache*). Para
salvá-las, é necessário informar `saveimg=True` nos argumentos de entrada das
funções `ndbands()` e `clipnd()`.

Os valores médios de NDVI são exportados em formato CSV para o arquivo
**"ndvis.csv"**. É disponibilizada uma cópia do mesmo no
repositório para simples conferência.

#### Testes

Os testes foram realizados, e com êxito, em ambas versões **2** e **3** do
Python em distribuições para plataforma **_Windows-based O.S._** (*Build* 10)
**x86-64**. Pelas caracterísitcas de portabilidade da linguagem, é esperado
sucesso também em testes realizados sobre plataformas *Linux-based O.S.*.
Eventuais inconsistências e exceções podem estar atreladas a parâmetros de
compilação de determinadas bibliotecas binárias.
